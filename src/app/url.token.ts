import { InjectionToken } from '@angular/core';
import { environment } from '../environments/environment';

export const BASE_URL = new InjectionToken<string>('base_url', {
  providedIn: 'root',
  factory: () => environment.baseUrl
})
