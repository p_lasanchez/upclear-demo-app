import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './features/auth/auth/auth.component';


const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'products',
    loadChildren: () => import('./features/products/products.module').then(m =>  m.ProductsModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/auth'
  },
  {
    path: '**',
    redirectTo: '/auth'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
