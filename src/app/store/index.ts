import { combineReducers } from 'redux'
import { productReducer, ProductState, defaultProductState } from './products'

export interface AppState {
  productReducer: ProductState
}

export const defaultRootState = {
  productReducer: defaultProductState
}

export const rootReducer = combineReducers({
  productReducer
})
