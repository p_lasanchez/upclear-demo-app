import { PRODUCT } from './product.action'
import { Product } from 'src/app/models/product';

export interface ProductState {
  products: Product[]
  isLoaded: boolean
}

export const defaultProductState = {
  products: [],
  isLoaded: false
}

export const productReducer = (
  state: ProductState = defaultProductState, action
) => {
  switch (action.type) {
    case PRODUCT.GET_PRODUCTS:
      return { ...state, products: action.products, isLoaded: true }

    case PRODUCT.SELECT_PRODUCT:
      const nextProducts = Array.from(state.products)
      return { ...state, products: nextProducts.map(prod => {
          if (prod.id === action.id) {
            prod.selected = ! prod.selected
          }
          return prod
        })
      }

    default:
      return state
  }
}
