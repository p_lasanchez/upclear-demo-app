import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '..';
import { ProductService } from 'src/app/core/product.service';

export enum PRODUCT {
  GET_PRODUCTS = 'PRODUCT/GET_PRODUCTS',
  SELECT_PRODUCT = 'PRODUCT/SELECT_PRODUCT'
}

@Injectable({
  providedIn: 'root'
})
export class ProductAction {

  constructor(
    private ngRedux: NgRedux<AppState>,
    private productService: ProductService
  ) { }

  getProducts = () => {
    this.productService.getProducts().subscribe(products => {
      this.ngRedux.dispatch({
        type: PRODUCT.GET_PRODUCTS,
        products
      })
    })
  }

  selectProduct = (id: number) => {
    this.ngRedux.dispatch({
      type: PRODUCT.SELECT_PRODUCT,
      id
    })
  }
}
