import { NgModule } from '@angular/core';
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store'
import { AppState, rootReducer, defaultRootState } from './'
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [
    NgReduxModule
  ]
})
export class StoreModule {
  constructor(
    private ngRedux: NgRedux<AppState>,
    private devTools: DevToolsExtension
  ) {
    const enhancers = []
    if (!environment.production) {
      enhancers.push( devTools.isEnabled() ? devTools.enhancer() : (f) => f )
    }

    ngRedux.configureStore(
        rootReducer,
        defaultRootState,
        [], // middlewares
        enhancers
    )
  }
}
