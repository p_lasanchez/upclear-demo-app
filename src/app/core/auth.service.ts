import { Injectable, Inject } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BASE_URL } from '../url.token'

// import { baseUrl } from '../app.module'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    @Inject(BASE_URL) private url
  ) {}


  connectUser = ({ email, pass }: User): Observable<User[]> => {
    return this.http.get<User[]>(`${this.url}/users?email=${email}&pass=${pass}`)
  }
}
