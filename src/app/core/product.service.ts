import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { BASE_URL } from '../url.token'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient,
    @Inject(BASE_URL) private baseUrl
  ) {}

  getProducts = (): Observable<Product[]> => (
    this.http.get<Product[]>(`${this.baseUrl}/products`)
  )
}
