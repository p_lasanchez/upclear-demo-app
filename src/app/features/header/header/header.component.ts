import { Component } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';

function getProductCount(state) {
  return state.productReducer.products.filter(pr => pr.selected).length;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @select(getProductCount) counter$: Observable<number>
}
