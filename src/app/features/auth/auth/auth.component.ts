import { Component } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/core/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {
  user: User = {
    email: null,
    pass: null
  }

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  handleSubmit = (): Subscription => {
    return this.authService.connectUser(this.user).subscribe((users: User[]) => {
      if (users.length > 0) {
        this.router.navigateByUrl('/products')
      }
    })
  }
}
