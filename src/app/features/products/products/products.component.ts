import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductAction } from 'src/app/store/products/product.action';
import { Observable } from 'rxjs';
import { select, NgRedux } from '@angular-redux/store';
import { AppState } from 'src/app/store';

const getProducts = state => state.productReducer.products

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @select(getProducts) products$: Observable<Product[]>

  constructor(
    private productAction: ProductAction,
    private ngRedux: NgRedux<AppState>
  ) {}

  ngOnInit() {
    const isLoaded = this.ngRedux.getState().productReducer.isLoaded

    if (isLoaded) { return }
    this.productAction.getProducts()
  }

  handleSelect = (id: number) => {
    this.productAction.selectProduct(id)
  }
}
